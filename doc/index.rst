SIP Reference Guide
===================

.. toctree::
    :maxdepth: 2

    introduction
    examples
    command_line_tools
    legacy_command_line_tools
    specification_files
    directives
    annotations
    other_topics
    c_api
    pyproject_toml
    sipbuild_api
